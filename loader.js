'use strict';
var db = require('@fulfill/cage/mySQL');
var r = require('./lib/db').r;
var async = require('async');


var roles = [
	['admin'],
	['admin','seller'],
	['admin','buyer'],
	['admin','buyer','seller'],
	['seller'],
	['buyer'],
	['buyer','seller']
];

function cycle( list ){
	let len = list.length;
    let idx = 0

	return {
		next: function(){
	        return list[ idx++ % len ];	
		}
	}
}


var iter = cycle( roles );

var cargo = async.cargo(function( customers, cb ){
	console.log(`inserting ${customers.length} customers`);
	r.table('auth_user').insert( customers,{durability:'soft'} )
		.then( function( ){
			console.log('inserted');
			cb()
		})
		.catch(function(err){
			console.log('error', err);
			cb();
		})
}, 1000)

cargo.drain = function(){
	console.log('cargo is empty');
}

db
	.knex('customers')
	.orderBy('created_at','ASC')
	.stream( function( stream ){
		stream.on('data', function( customer ){
			cargo.push({
				created:r.now()
				,superuser: false
				,active: true
				,name:{
					first: customer.first_name
				  , last: customer.last_name
				}
				,email: customer.email
				,roles: iter.next()
			},console.log);
		});

		stream.on('end', function(){
			console.log("stream drained");
		});
	})
