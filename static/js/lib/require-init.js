var app = require({
	baseUrl:"static/js/modules"
	,urlArgs:(+new Date)
	,paths:{
		"mootools":"../lib/mootools"
		,"mooml":"../lib/mooml"
	}
	,shim:{
		"mootools":{
			exports:"mootools"
		}
		,"mooml":{
			exports:"Mooml"
			,deps:["mootools"]
		}
	}
})
