/*jshint laxcomma:true, smarttabs: true, mootools: true, -W004:true */
/**
 * container view for rendering a list sub template
 * @module NAME
 * @author Eric Satterwhite
 **/
define([
	'require'
	,'module'
	,'exports'
	,'view'
	,'mootools'
	,'mooml'
],function(require, module, exports, View /* mootools mooml */){
	var List;
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	List = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:View
		,options:{
			renderTo:null
			,name:'users'
			,data:null
			,tpl: function( data ){
				var tags = Mooml.engine.tags;
				var data = Array.from( data );

				for(var idx=0, len=data.length; idx < len; idx++ ){
					var user = data[idx];

					tags.li({
						text: user.name.first
					});
				}
			}
		}
	});

	return List;
});
