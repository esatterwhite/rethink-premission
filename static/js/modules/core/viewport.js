/*jshint laxcomma:true, smarttabs: true */
/**
 * Viewport module
 * @exports Viewport
 * @module module:core/views/viewport
 * @author Eric Satterwhite
 * @requires module:mvc/view
 * @requires module:core/views/nav
 * @requires mooml
 **/
define([
	'require'
	,'exports'
	,'module'
	,'view'
	,'mooml'
],function(require, module, exports, View /* Mooml */ ){
	var Viewport;
	/**
	 * Primary viewport for holding sub views
	 * @class module:core/views/viewport.Viewport
	 * @extends module:mvc/view.View
	 * @param {Object} options configuration options for the class instance
	 * @param {Boolean} [options.override=true] if set to true will empty out parent element before rendering
	 */
	Viewport = new Class({
		Extends:View
		,override: true
		,options:{
			renderTo: document.body
			,tpl:function(){
				var tags = Mooml.engine.tags;

				tags.section({'id':'viewport', 'class':'row'});
			}
		}

		,render: function(){
			this.parent();
		}
	});

	return Viewport;
});
