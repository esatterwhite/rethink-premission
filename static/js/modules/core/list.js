/*jshint laxcomma:true, smarttabs: true, mootools: true, -W004:true */
/**
 * container view for rendering a list sub template
 * @module NAME
 * @author Eric Satterwhite
 **/
define([
	'require'
	,'module'
	,'exports'
	,'view'
	,'mootools'
	,'mooml'
],function(require, module, exports, View /* mootools mooml */){
	var List;
	/**
	 * DESCRIPTION
	 * @class module:NAME.Thing
	 * @param {TYPE} NAME DESCRIPTION
	 * @example var x = new NAME.Thing({});
	 */
	List = new Class(/** @lends module:NAME.Thing.prototype */{
		Extends:View
		,options:{
			renderTo:null
			,name:'list'
			,data:[{"class":'fake'}]
			,tpl: function( data ){
				var tags = Mooml.engine.tags;
				var data = data ||{};
				var max = data.max || 4;
				var names = ['Sam', 'Doug', "ralph", "ricky"]
				tags.ul({
					'class':'small-block-grid-1 medium-block-grid-{max} large-block-grid-{max}'.substitute({max:max})
				})
			}
		}
	});

	return List;
});
