/*jshint laxcomma:true, smarttabs: true, -W098:true, -W030: true */
/**
 * Provides the view layer for the mvc module
 * @module module:mvc/view
 * @author eric.satterwhite
 * @exports View
 * @requires mooml
 **/
define([
	'require'
	,'module'
	,'exports'
	,'mooml'
], function( require, module, exports /* Mooml */ ){
		var View;
		/**
		 * View Class which renders templates and manages sub templates
		 * @class module:NAME.Thing
		 * @param {TYPE} NAME DESCRIPTION
		 * @example var x = new NAME.Thing({});
		 */
		View = new Class(/** @lends module:NAME.Thing.prototype */{
			Implements:[Events, Options, Mooml.Templates ]
			,options:{
				renderTo:null
				,tpl:Function.from()
				,name:null
				,override:false
				,data:null
			}
			,initialize: function( options ){
				var code;

				this.setOptions( options );
				this.name = this.options.name || String.uniqueID();
				this.registerTemplate( this.name, this.options.tpl );
				Mooml.register( this.name, this.options.tpl );
			}
			/**
			 * renders a view template
			 * @method module:mvc/view#render
			 * @param {Object} [data] the data to pass to the template rendering
			 * @param {object} [bind] the object of which scope to execute the tempate function in
			 * @return Element rendered html element from template parsing
			 **/
			,render: function( data, bind ){
				var out;
				data = data || this.options.data || {};
				if( this.rendered ){
					return this.element;
				}
				// let people know we are rendering
				/**
				 * @name moduleName.Thing#shake
				 * @event
				 * @param {Event} e
				 * @param {Boolean} [e.withIce=false]
				 */
				this.fireEvent('beforerender', [this, data]);
				out = this.renderTemplate( this.name, data, bind || this );
				/**
				 * @name moduleName.Thing#shake
				 * @event
				 * @param {Event} e
				 * @param {Boolean} [e.withIce=false]
				 */
				this.fireEvent('afterrender',[this, data, out]);

				// store a bit of state
				this.element = out;
				this.rendered = true;

				// if we were given a location
				// put the view there
				if( this.options.renderTo ){
					var el = $(this.options.renderTo);
					if( this.options.override){
						el.empty();
					}

					el.adopt( this.element );
				}
				this.fireEvent('render',[ this, data, out ]);
				return out;
			}

			/**
			 * add a view to the current view instance as a sube view
			 * @method module:mvc/view#add
			 * @param {View|String} An instance of a view or a string path to a module that returns a view instance
			 * @param {Object} [config]
			 * @return {Element}
			 **/
			,add: function(view, config){
				var tpl
					,that = this;

				var where = 'bottom';


				if( typeOf( view ) === "string" ){
					require([view], function( cls ){
						tpl = that.repeat( cls, config );
						that.toElement().adopt( tpl ,  where );
					});
				} else{
					tpl = $(view);
						this.toElement().adopt( tpl ,  where );
				}

				return this.toElement();
			}

			,repeat: function( view, config, where ){

				return new Elements( Array.from(config).map( function(cfg){
					return new view().render( cfg );
				}));

			}

			,toElement: function(){
				return this.element ? this.element : this.render();
			}

			/**
			 * removes all elements from the current views rendered container elements
			 * @chainable
			 * @method module:mvc/view.View#removeAll
			 * @return View The current view instance
			 **/
			, removeAll: function( ){
				this.element && this.element.empty();
				return this;
			}
			,$family: function( ){
				return "object";
			}

		});

		return View;
	}
);
