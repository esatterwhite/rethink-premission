'use strict';

define([
		'module'
		,'exports'
		,'mootools'
	],function( module, exports, mootools ){
		var Tree =  new Class({
			Implements: [ Class.Options, Class.Events ]
			,options:{
				url:'http://localhost:3001/api/v1/users'
			  , onClick: Function.from()
			}
			
			,initialize:function( element, options ){
				this.element = element;
				this.setOptions( options );
				this.load( );
			}

			,toElement: function(){
				return this.element;
			}

			,attach: function(){
				this.element.addEvents({
					"click:relay(li)":function( evt ){
						evt.stopPropgation( );
					}.bind( this )
				})
			}

			,load: function( ){
				new Ajax.JSON({
					url:this.options.url
					,onSuccess: function( data ){
						this.render( data );
					}.bind( this )
				}).send();
			}
		});
		module.exports = Tree;
});
