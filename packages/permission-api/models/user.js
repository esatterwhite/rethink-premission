
var db = require('../../../lib/db');
var type = db.type;
var Role = require('./role')
var r = db.r;
var User;

module.exports = User = db.createModel('auth_user',{
    name: type.object().schema({
        first:type.string().required()
      , last: type.string().required()
    })
    ,email: type.string().email().required()
    ,created: type.date().default(db.r.now())
    ,superuser:type.boolean().default(false)
    ,vip:type.boolean().default(false)
    ,max_auto_charge:type.number().default(0)
    ,compy:type.string().default(null)
    ,active:type.boolean().default(true)
    ,website:type.string().default(null)
    ,is_affiliate:type.boolean().default(false)
    ,upsells_enabled:type.boolean().default(false)
    ,phone:type.object().schema({
        home:type.string().default(null)
       ,work:type.string().default(null)
       ,cell:type.string().default(null)
    })
    ,roles:[type.string()]
    ,registered:type.boolean().default(false)
},{
    pk:'auth_user_id'
});


User.defineStatic('withPermissions', function( ){
  return this.merge(function( doc ){
    return r.object(
      'permissions', 
      doc("roles").eqJoin(function( id ){
         return id
      }, r.db('allstar').table( 'auth_role')).zip()('permissions')
      .reduce(function(left, right ){
        return left.merge(right )
      })  
    ) 
  })
})

