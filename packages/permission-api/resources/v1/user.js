/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * user.js
 * @module user.js
 * @author 
 * @since 0.0.1
 * @requires tastypie-rethink
 * @requires moduleB
 * @requires moduleC
 */

var Resource = require( 'tastypie-rethink' )
  , models = require("../../../../lib/loading/models")
  , User = models.load()['permission-api'].user
  , UserResource
  ;

/**
 * Description
 * @constructor
 * @alias module:user.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('user.js');
 */

UserResource = Resource.extend({
	options:{
		name:'user'
		,pk:'auth_user_id'
		,queryset: User.filter({})
		,allowed:{
			list:{get:true},
			detail:{get:true}
		}

	}
	,fields:{
		name:{type:'object'}
		,email:{type:'char'}
		,created:{type:'date'}
		,roles:{type:'array'}
		,permissions:{type:'object', default:()=>{ return {}; }}
	}

	,constructor: function( options ){
		this.parent('constructor', options)
	}

	, get_object: function( bundle, callback ){
		this.options
			.queryset
			._model
			.get( bundle.req.params.pk )
			.withPermissions()
			.run(callback);

		return this;
	}
});
module.exports = UserResource;
