var Hapi = require('hapi')
  , path = require('path')
  , tastypie = require('tastypie')
  , Api = tastypie.Api
  , conf = require('keef')
  , Loader = require('gaz/fs/loader')
  , logger = require('bitters')
  , Nunjucks = require('nunjucks')
  , server
  , v1
  , resources
  , PORT
  ;


  PORT = conf.get('PORT')
  module.exports = server = new Hapi.Server({})

  resources = new Loader({
  	filepattern:/\.js$/
  	,searchpath:'resources'
  	,recurse: true
  });

  v1 = new Api('api/v1');

  const ui = server.connection({
		host:'0.0.0.0',
		port:PORT,
		labels:['web']
  });

  const api = server.connection({
		host:'0.0.0.0',
		port:~~PORT + 1,
		labels:['api'],
		routes:{
			cors:true
		}
  });

resources
	.load()
	.flat()
	.forEach(function( resource ){
		v1.use( new resource() );
	})


server.register([ v1 ], {select:'api'}, console.log )

server.register([
	require('inert'),
	require('vision')
], function( err ){
	if(err){
		logger.error( err );
		process.exit(1)
	}
	server.views({
		engines:{
			njhtml:{
				compile: function( src, options ){
					var template = Nunjucks.compile( src, options.environment);
					return function( ctx ){
						return template.render( ctx );
					}
				}
			  , prepare: function( options, next ){
					options.compileOptions.environment = Nunjucks.configure( options.path,{watch:false});
					return next()
			  }
			}
		}
	   ,path: path.join( __dirname, 'templates' )
	});

	ui.route({
		method:'GET'
	  , path:'/'
      , handler: function( req, reply ){
	  		reply.view('index',{});
	  }
	});
	ui.route({
        method:'GET'
      , path:'/static/{param*}'
      , handler:{
            directory:{
                path:'./static'
               ,redirectToSlash: true
               ,index:true
            }
        }
    });

    ui.route({
        method:'GET'
      , path:'/components/{param*}'
      , handler:{
            directory:{
                path:'./bower_components'
               ,redirectToSlash: true
               ,index:false
            }
        }
    });

	server.start(function( err ){
		if( err ){
			logger.error( err );
			process.exit( 1 );
		}
		logger.info('ui server running at: %s', ui.info.uri);
		logger.info('api server running at: %s', api.info.uri);
	});
})
