/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict';
/**
 * models.js
 * @module models.js
 * @author Eric Satterwhite 
 * @since 1.0.0
 * @requires gaz/fs/loader
 * @requires gaz/class
 */

var path    = require('path')
  , Loader  = require( 'gaz/fs/loader' )
  , Class   = require( 'gaz/class' )
  , attempt = require( 'gaz/function' ).attempt // try/catch wrapper
  , set     = require('gaz/object').set
  , values     = require('gaz/object').values
  , compact = require( 'gaz/array' ).compact    // standard compact function
  , clone   = require( 'gaz/lang' ).clone       // standard clone function
  , isEmpty = require( 'gaz/lang' ).isEmpty       // standard isEmpty function

  , Models
  ;

/**
 * Description
 * @constructor
 * @alias module:models.js
 * @param {TYPE} [param]
 * @param {TYPE} [?param.val=1j]
 * @example var x = new require('models.js');
 */
Models = new Class({
	inherits: Loader
	,options:{
		filepattern:/\.js$/
		,searchpath:'models'
		,recurse:true
	}

	,load: function load( ){
		var packages = this.find.apply(this, arguments)
		  , obj = {}
		  ;

		Object.defineProperty(obj,'flat',{
			value:this.toArray
		});

		for( let key in packages ){
			this.cache[key] = this.cache[key] || {} ;

			if( !this.cache[key].length ){
				for(let loaded of packages[key] ){

					let result = this.remap( loaded );
					result && set(this.cache[key], path.parse( loaded.path ).name, result );   
				}
			}

			obj[key] = clone( this.cache[key]);
		}

		return obj;
	}

	,toArray: function(){
		let out = []
		for(let app in this ){
			out = out.concat( values ( this[app] ) )
		}
		return out;
	}
})


const loader = new Models();

exports.find = function(){
	return loader.find.apply( loader, arguments )
}
exports.load = function(){
	return loader.load.apply( loader, arguments );
}
exports.Loader = Models;