'use strict';

var fs = require('fs');
var words = null;
var getRandom = require('gaz/array').getRandom;
var path = require('path');
const LOCATION = 'fixtures/words.txt'
const TRIM = /\'\w+/;

exports.random = function( count ){
	return new Promise(function( resolve, reject ){
		count = (~~count ) || 1;
		let idx = 0;
		let value = []
		if( !words ){
			let data = fs.readFileSync( path.resolve( path.join( __dirname, '..', LOCATION ) ) );
			words = data.toString('ascii').split( '\n');
		}

		while( idx++ < count ){
			console.log( idx, count)
			value.push( getRandom( words ).replace(TRIM, '') );
		}
		console.log( 'resolving' )
		return resolve( value.join(' ') );
	});
}
